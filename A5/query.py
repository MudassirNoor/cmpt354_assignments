from flask import Flask, g, request, jsonify
import pyodbc
from connect_db import connect_db
import time, datetime


app = Flask(__name__)

@app.route('/')
def hello_world():
    return 'Hello, World!'

def get_db():
    """Opens a new database connection if there is none yet for the
    current application context.
    """
    if not hasattr(g, 'azure_db'):
        g.azure_db = connect_db()
        g.azure_db.autocommit = True
        g.azure_db.set_attr(pyodbc.SQL_ATTR_TXN_ISOLATION, pyodbc.SQL_TXN_SERIALIZABLE)
    return g.azure_db

@app.teardown_appcontext
def close_db(error):
    """Closes the database again at the end of the request."""
    if hasattr(g, 'azure_db'):
        g.azure_db.close()



@app.route('/login')
def login():
    username = request.args.get('username', "")
    password = request.args.get('password', "")
    cid = -1
    #print (username, password)
    conn = get_db()
    #print (conn)
    cursor = conn.execute("SELECT * FROM Customer WHERE username = ? AND password = ?", (username, password))
    records = cursor.fetchall()
    #print records
    if len(records) != 0:
        cid = records[0][0]
    response = {'cid': cid}
    return jsonify(response)



@app.route('/getRenterID')
def getRenterID():
    """
       This HTTP method takes mid as input, and
       returns cid which represents the customer who is renting the movie.
       If this movie is not being rented by anyone, return cid = -1
    """
    mid = int(request.args.get('mid', -1))

    cid = -1

    conn = get_db()
    cursor = conn.execute("SELECT cid FROM Rental WHERE mid = ? AND status='open';", mid)
    record = cursor.fetchall()

    if len(record) != 0:
        cid = record[0][0]
    
    response = {'cid': cid}
    return jsonify(response)



@app.route('/getRemainingRentals')
def getRemainingRentals():
    """
        This HTTP method takes cid as input, and returns n which represents
        how many more movies that cid can rent.

        n = 0 means the customer has reached its maximum number of rentals.
    """
    cid = int(request.args.get('cid', -1))
    conn = get_db()

    maxMovies = 0
    moviesRented = 0

    # Tell ODBC that you are starting a multi-statement transaction
    conn.autocommit = False

    moviesRentedQuery = """SELECT COUNT(*) FROM Rental WHERE cid = ? AND status = 'open'; """
    maxMoviesQuery = """SELECT rp.max_movies FROM RentalPlan rp, Customer c WHERE rp.pid = c.pid AND c.cid = ?;"""
    
    cursor = conn.execute(moviesRentedQuery, cid)
    record = cursor.fetchall()

    if len(record) != 0:
        moviesRented = record[0][0]
    
    cursor = conn.execute(maxMoviesQuery, cid)
    record = cursor.fetchall()

    if len(record) != 0:
        maxMovies = record[0][0]

    conn.autocommit = True

    n = maxMovies - moviesRented

    response = {"remain": n}
    return jsonify(response)



def currentTime():
    ts = time.time()
    return datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')


@app.route('/rent')
def rent():
    """
        This HTTP method takes cid and mid as input, and returns either "success" or "fail".

        It returns "fail" if C1, C2, or both are violated:
            C1. at any time a movie can be rented to at most one customer.
            C2. at any time a customer can have at most as many movies rented as his/her plan allows.
        Otherwise, it returns "success" and also updates the database accordingly.
    """
    cid = int(request.args.get('cid', -1))
    mid = int(request.args.get('mid', -1))

    conn = get_db()
     # Tell ODBC that you are starting a multi-statement transaction
    conn.autocommit = False

    maxMovies = 0
    moviesRented = 0
    isMovieRented = False
    status = "success"

    isMovieRentedQuery = """SELECT COUNT(*) FROM Rental WHERE mid = ? AND status = 'open';"""
    insertQuery = """INSERT INTO Rental VALUES (?, ?, ?, ?);"""
    maxMoviesQuery = """SELECT rp.max_movies FROM RentalPlan rp, Customer c WHERE rp.pid = c.pid AND c.cid = ?;"""
    moviesRentedQuery = """SELECT COUNT(*) FROM Rental WHERE cid = ? AND status = 'open'; """

    cursor = conn.execute(isMovieRentedQuery, mid)
    record = cursor.fetchall()

    if len(record) != 0:
        isMovieRented = record[0][0] > 0

    cursor = conn.execute(insertQuery, (cid, mid, currentTime(), 'open'))

    cursor = conn.execute(moviesRentedQuery, cid)
    record = cursor.fetchall()

    if len(record) != 0:
        moviesRented = record[0][0]
    
    cursor = conn.execute(maxMoviesQuery, cid)
    record = cursor.fetchall()

    if len(record) != 0:
        maxMovies = record[0][0]

    remainingPlan = maxMovies - moviesRented

    print("Rented: " + str(moviesRented) + ", Rental plan: " + str(maxMovies) + ", Movie rented? " + str(isMovieRented)) 

    if (remainingPlan < 0 or isMovieRented):
        conn.rollback()
        print("Rollback done")
        status = "fail"
  
    conn.autocommit = True

    response = {"rent": status}

    #response = {"rent": "success"} OR response = {"rent": "fail"}
    return jsonify(response)

